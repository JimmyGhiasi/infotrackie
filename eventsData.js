{
    "upcomingEvents":[

        {
            "day": "27",
            "month" : "May",
            "destination": "http://www.infotrack.com.au/events/PEXA_Update_3.htm",
            "name": "Lunchtime seminar: PEXA update. Presented by Sally Kite, PEXA Executive Account Manager, Practitioner Service, Matthew Perriam, InfoTrack, General Manager, Products and Paul Watkins, Stewart Title Limited, General Counsel, Australia. "
        },

        {
            "day": "10",
            "month" : "June",
            "destination": "http://www.infotrack.com.au/events/shah.html",
            "name": "Lunchtime seminar: PPSA and the Sale of Business, presented by Shah Rusiti, Partner, Teece Hodgson & Ward"
        },
        {
            "day": "10",
            "month" : "Sep",
            "destination": "http://www.infotrack.com.au/get-involved/events.htm",
            "name": "2015 ALPMA/InfoTrack Thought Leadership Awards, presented at the 2015 ALPMA Summit. Nominations open mid May."
        }

],
    "webinars":[
        {
            "day": "13",
            "month" : "May",
            "destination": "http://www.infotrack.com.au/events/tonyCahill-webinar.html",
            "name": "Lunctime: eConveyancing and the 2014 NSW Contract of Sale & Purchase of Land, presented by Tony Cahill"
        },

        {
            "day": "20",
            "month" : "May",
            "destination": "https://attendee.gotowebinar.com/register/6325940458668154882",
            "name": "Lunchtime: PEXA and InfoTrack working together - QLD - free of charge "
        },
        {
            "day": "3",
            "month" : "June",
            "destination": "https://attendee.gotowebinar.com/register/513561354430313986",
            "name": "Lunchtime: PEXA and InfoTrack working together - NSW - free of charge "
        },
        {
            "day": "17",
            "month" : "June",
            "destination": "https://attendee.gotowebinar.com/register/7937102125848332546",
            "name": "Lunchtime: PEXA and InfoTrack working together - VIC - free of charge"
        }



],
    "news":[
        {
            "day": "",
            "month" : "May",
            "destination": "http://www.infotrack.com.au/products-services/nsw-civil-registry-efiling.htm",
            "name": "NSW Civil Registry e-Filing service - 12 new court forms added. Lodge and pay for documents from the convenience of your desk for any local, district or supreme registry court."
        } ,
        {
            "day": "",
            "month" : "May",
            "destination": "https://au.finance.yahoo.com/news/rba-says-too-much-focus-140230106.html",
            "name": "The Reserve Bank of Australia says the Sydney property market gets far too much attention, and that further interest rate cuts are possible."
        } ,
         
        {
            "day": "",
            "month" : "May",
            "destination": "http://www.infotrack.com.au/reveal",
            "name": "REVEAL now available- data visualiation of your search results. Free to all InfoTrack clients "
        } 


]
}