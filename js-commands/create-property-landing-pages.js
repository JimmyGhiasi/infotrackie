var fs = require('fs');
var _ = require('lodash');
var jade = require('jade');

var file = 'data.json';


fs.readFile(file, 'utf8', function (err, data) {
  if (err) {
    console.log('Error: ' + err);
    return;
  }

  data = JSON.parse(data);


var pageName =  data.pages[0].pageName;
var pageTitle =  data.pages[0].pageTitle;
var heroHeading =  data.pages[0].heroHeading;
var heroTagLine =  data.pages[0].heroTagline;

console.log(pageTitle);

var html = jade.renderFile ( 'property-landing-page.jade', { title : pageTitle, heroHeading : heroHeading, heroTag : heroTagLine});

fs.writeFile('../products-services/'+ pageName +'.html', html, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
}); 

});