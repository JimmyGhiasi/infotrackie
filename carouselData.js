{
    "carousels":[
        {
            "heading": "REVEAL-Visualisation of your search results",
            "parag" : "Free tool for all InfoTrack clients.",
            "cta": "Watch demo",
            "uAddress" : "http://www.infotrack.com.au/reveal"

        },
        {
            "heading": "NSW e-Filing Court Service",
            "parag": "No more lodging documents across the counter for any local, district or supreme registry court.",
            "cta": "Find out more",
            "uAddress" : "http://www.infotrack.com.au/products-services/nsw-civil-registry-efiling-company.htm"
        },
        {
            "heading": "Back by popular demand",
            "parag": "‘eConveyancing and the 2014 NSW Contract of Sale & Purchase of Land’ lunchtime webinar.",
            "cta": "Book now",
            "uAddress" : "http://www.infotrack.com.au/events/tonyCahill-webinar.html"
        },
        {
            "heading": "2015 ALPMA/InfoTrack Thought Leadership Awards.",
            "parag": "Nominations open mid-May.",
            "cta": "Find out what kind of project is eligible",
            "uAddress" : "http://www.alpma.com.au/Summit/thought_leadership_awards"
        }


]
}